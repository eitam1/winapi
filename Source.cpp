#include <iostream>
#include <Windows.h>
#include <vector>

#include "Helper.h"
using namespace std;

void main()
{
	vector<string> command;
	string words;
	char buffer[MAX_PATH];
	while (1)
	{
		
		cout << ">>";
		getline(cin,words);
		Helper::trim(words);
		command = Helper::get_words(words);
		if (command[0] == "pwd")
		{	
			GetModuleFileName(NULL, buffer, MAX_PATH);
			cout << buffer;
		}
		if (command[0] == "cd")
		{
			if (!SetCurrentDirectory(command[1].c_str()))
			{
				cout << "FAIL";
			}
		}
		if (command[0] == "create")
		{
			if (!CreateDirectory(command[1].c_str(), NULL))
			{
				cout << "ERROR";
				if (ERROR_ALREADY_EXISTS == GetLastError())
				{
					RemoveDirectory(command[1].c_str());
					CreateDirectory(command[1].c_str(), NULL);
				}
			}
		}
	}
		system("pause");
}


